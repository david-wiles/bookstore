const { hasPermission } = require('../util/permission');
const { User } = require('../database/sequelize');
const { query } = require('../database/db');
const passport = require('passport');
const { Strategy: LocalStrategy } = require('passport-local');
const bcrypt = require('bcrypt');


// Login middleware
exports.isAuthenticated = (req, res, next) => {

  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/users/login');
};

exports.isAuthorized = async (req, res, next) => {

  if (req.isAuthenticated()) {

    let authorized = await hasPermission(req.user, req.path, req.method);
    if (authorized) return next();

  }

  req.flash('errors', 'Unauthorized.');
  res.redirect('/users/login');

};

exports.isAdmin = async (req, res, next) => {

  if (req.isAuthenticated()) {

    let sql =
      `SELECT auth_groups.name ` +
      `FROM users ` +
      `LEFT JOIN auth_groups on users.auth_group_uuid = auth_groups.uuid ` +
      `WHERE users.uuid = $1;`;

    let result = await query(sql, [req.user]);

    if (result.rows[0]['name'] === 'Admin') return next();

  }

  req.flash('errors', 'Unauthorized');
  res.redirect('/users/login');

};

passport.serializeUser((user, done) => {
  done(null, user.uuid);
});

passport.deserializeUser((id, done) => {
  done(null, id);
});

passport.use(new LocalStrategy({ usernameField: 'email'}, (email, password, done) => {

  User.findOne({ where: { email: email }})
    .then(user => {
      if (!user) { return done(null, false, { msg: 'User not found' }) }
      else {

        bcrypt.compare(password, user.dataValues.password_hash, (err, res) => {
          if (err) {
            return flash('errors', 'Something went wrong.');
          } else if (res === false) {
            return done(null, false, { msg: 'Invalid email or password.' });
          } else {
            return done(null, user);
          }
        });

      }
    })
    .catch(err => done(null, false, { msg: 'An error occurred.' }))
}));
