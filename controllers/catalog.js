// Book catalog controller
const { check, validationResult } = require('express-validator');
const { Book, Author } = require('../database/sequelize');
const { query } = require('../database/db');


exports.catalog = async (req, res) => {

  let books = {};
  let sql = "SELECT uuid, title, author, image FROM books LIMIT 10;";
  let results = await query(sql);

  if (results)
    books = results.rows;
  else
    req.flash("errors", "Could not find featured books");

  res.render('catalog/catalog', { title: 'Catalog', books: books });
};

exports.search = async (req, res) => {
  let text = req.query.text;

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.redirect('/');
  } else {

    let sql = "SELECT uuid, title FROM books WHERE title ILIKE $1;";

    try {
      let result = await query(sql, ["%" + text + "%"]);
      return res.render('catalog/search', { results: result.rows });
    } catch (err) {
      console.log(error);
    }

  }

};

exports.bookIndex = (req, res) => {
  res.render('catalog/bookIndex', { title: 'Authors' });
};

exports.bookDetail = (req, res) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.redirect('/catalog/book');
  } else {

    Book.findByPk(req.params.id)
      .then(row => {
        let data = row.dataValues;
        delete data.other;
        delete data.createdAt;
        delete data.updatedAt;
        res.render('detail/bookDetail', { title: data.title, book: data });
      })
      .catch(err => {
        req.flash('errors', 'Something went wrong.');
        return res.redirect('/catalog/book');
      });

  }

};

exports.authorIndex = (req, res) => {
  res.render('catalog/authorIndex', { title: 'Authors' });
};

exports.authorDetail = (req, res) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.redirect('/catalog/author');
  } else {

    Author.findByPk(req.params.uuid)
      .then(row => {
        let data = row.dataValues;
        delete data.uuid;
        delete data.other;
        delete data.createdAt;
        delete data.updatedAt;
        res.render('detail/authorDetail', { title: data.name, author: data });
      })
      .catch(err => {
        req.flash('errors', 'Something went wrong.');
        res.redirect('/catalog/author');
      });

  }

};

exports.keyword = (req, res) => {
  res.render('catalog/keyword', { title: 'Genres' })
};

exports.keywordDetail = async (req, res) => {
  // TODO implement with model

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.redirect('/keyword');
  } else {

    try {

      let sql = "SELECT * FROM book_keywords WHERE uuid = $1;";

      let results = await query(sql, [req.params.uuid]);
      if (results.rows.length === 0) {
        keyword = results.rows[0];
        res.render('keywordDetail', {title: keyword.name, keyword: keyword});
      }

    } catch (err) {
      console.error(err);
    }
  }

};