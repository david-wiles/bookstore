const { check, validationResult } = require('express-validator');
const { query } = require('../database/db');


exports.autocomplete = async (req, res) => {

  let text = req.query.text;

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let sql = "SELECT uuid, title FROM books WHERE title ILIKE $1 OR author ILIKE $1;";
    let values = text + '%';

    try {

      let text = await query(sql, [values]);
      return res.send(text.rows);

    } catch (err) {
      console.error(err);
    }

  }

};