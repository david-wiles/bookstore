// Authentication view controller
const { validationResult } = require('express-validator');
const { User } = require('../database/sequelize');
const passport = require('passport');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');


exports.loginGet = (req, res) => {
  res.render('user/login', { title: 'Login' })
};

exports.loginPost = (req, res, next) => {
  return passport.authenticate('local', (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      req.flash('errors', info);
      return res.redirect('/users/login');
    }
    req.logIn(user, (err) => {
      if (err) { return next(err); }
      req.flash('success', { msg: 'Logged In!' });
      res.redirect(req.session.returnTo || '/');
    })
  })(req, res, next);
};

exports.logout = (req, res) => {
  req.logout();
  req.session.destroy((err) => {
    if (err) flash('errors', 'Could not log out.');
    req.user = null;
    res.redirect('/');
  });
};

exports.registerGet = (req, res) => {
  res.render('user/register', { title: 'Register' })
};

exports.registerPost = (req, res, next) => {

  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    req.flash('errors', errors.array());
    return res.redirect('/users/register');
  } else {

    User.findOne({ where: { email: req.body.email } })
      .then(user => {
        if (user) {
          req.flash('errors', 'User with that email already exists. Forgot password?');
          return res.redirect('/users/login');
        } else {
          bcrypt.hash(req.body.password, 12)
            .then(hash => {
              User.create({
                email: req.body.email,
                password_hash: hash,
              })
                .then(user => {
                  return req.logIn({
                    uuid: user.dataValues.uuid,
                    email: user.dataValues.email,
                  }, (err) => {
                    if (err) return next(err);
                    req.flash('success', { msg: 'Account Created!' });
                    res.redirect('/');
                  });
                })
                // Insert error
                .catch(err => next(err));
            })
            // Hashing error
            .catch(err => next(err));
        }
      })
      // Query error
      .catch(err => next(err));

  }
};

exports.accountGet = (req, res, next) => {

  User.findByPk(req.user)
    .then(user => {
      let data = user.dataValues;
      delete data.uuid;
      delete data.password_hash;
      delete data.createdAt;
      delete data.updatedAt;
      delete data.auth_group_uuid;
      let names = {};
      for(prop in data) { names[prop] = prop.replace(/_/g, ' ') }
      return res.render('user/account', { title: 'Account', user: data, input_fields: names })
    })
    .catch(err => next(err))

};

exports.accountPut = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let user = await User.update( req.body,
      { where: { uuid: req.user} }
    );

    res.send({ user: user });

  }

};

exports.forgotPassword = (req, res) => {

  res.render("user/forgotPassword", { title: "Password Reset" })

};

exports.sendEmail = (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let transporter = nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      secure: true,
      auth: {
        user: process.env.EMAIL_AUTH_USER,
        pass: process.env.EMAIL_AUTH_PASS,
      }
    });

    let mail = {
      to: req.body.address,
      subject: "BOOKSTORE password reset",
      body: '',
    };

    transporter.sendMail(mail, (error, info) => {
      if (error)
        return console.error(error);
      console.log(info.messageId, info.response);
    });

    res.render("user/checkEmail", { title: "Password Reset" });

  } else {

    req.flash("errors", "Something went wrong");
    res.redirect("/");

  }

};
