const { check, validationResult } = require('express-validator');

// Include all validation chains here

const registerValidation = [
    check('email')
        .isEmail().withMessage('Email must be valid.'),
    check('password')
        .isLength({ min: 8 }).withMessage('Password must be at least 8 characters.')
        .isAscii().withMessage('Password cannot contain special characters.'),
    // TODO check if user exists in DB

];

module.exports = registerValidation;