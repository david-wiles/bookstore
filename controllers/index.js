// Index view controller

exports.index = (req, res) => {
  res.render('index', { title: 'Home', user: req.user })
};

