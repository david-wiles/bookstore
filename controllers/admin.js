// Admin controller
const { validationResult } = require('express-validator');
const { User, Group, Book, Author } = require('../database/sequelize');
const { query } = require('../database/db');


exports.getIndex = (req, res) => {
  return res.render('adminIndex', { title: 'Admin Home', admin: true });
};

exports.getBooks = async (req, res) => {

  let sql = "SELECT uuid, title FROM books ORDER BY title;";
  let result = await query(sql);

  return res.render('admin/bookList', { title: 'Books', admin: true, books: result.rows, type: 'book' });

};

exports.bookDetail = (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    Book.findByPk(req.params.uuid)
      .then(result => {
        let book = result.dataValues;
        let uuid = book.uuid;
        delete book.uuid;
        let names = {};
        for(prop in book) { names[prop] = prop.replace(/_/g, ' ') }
        return res.render('admin/detail/bookDetail', {
          title: book.title,
          admin: true, book: book,
          input_fields: names,
          uuid: uuid
        });
      })
      .catch(err => console.error(err));

  }

};

exports.getAuthors = async (req, res) => {

  let sql = "SELECT uuid, name FROM authors ORDER BY name;";
  let result = await query(sql);

  return res.render('admin/authorList', { title: 'Authors', admin: true, authors: result.rows, type: 'author' });
};

exports.authorDetail = (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    Author.findByPk(req.params.uuid)
      .then(result => {
        let author = result.dataValues;
        let uuid = author.uuid
        delete author.uuid;
        let names = {};
        for(prop in author) { names[prop] = prop.replace(/_/g, ' ') }
        return res.render('admin/detail/authorDetail', {
          title: author.name,
          admin: true,
          author: author,
          input_fields: names,
          uuid: uuid
        });
      })
      .catch(err => console.error(err));

  }

};

exports.getUsers = async (req, res) => {

  let sql = "SELECT uuid, email FROM users ORDER BY email;";
  let result = await query(sql);

  return res.render('admin/usersList', { title: 'Users', admin: true, users: result.rows, type: 'user' });
};

exports.userDetail = (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    User.findByPk(req.params.uuid)
      .then(result => {
        let user = result.dataValues;
        let uuid = user.uuid;
        delete user.uuid;
        delete user.password_hash;
        let names = {};
        for(prop in user) { names[prop] = prop.replace(/_/g, ' ') }
        return res.render('admin/detail/userDetail', {
          title: user.email,
          admin: true,
          user: user,
          input_fields: names,
          uuid: uuid
        });
      })
      .catch(err => console.error(err));

  }

};

exports.getGroups = async (req, res) => {

  let sql = "SELECT uuid, name FROM auth_groups ORDER BY name;";
  let result = await query(sql);

  res.render('admin/groupsList', { title: 'Groups', admin: true, groups: result.rows, type: 'auth_group' });
};

exports.groupDetail = (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    Group.findByPk(req.params.uuid)
      .then(result => {
        let group = result.dataValues;
        let uuid = group.uuid;
        delete group.uuid;
        let names = {};
        for(prop in group) { names[prop] = prop.replace(/_/g, ' ') }
        return res.render('admin/detail/groupDetail', {
          title: group.name,
          admin: true,
          group: group,
          input_fields: names,
          uuid: uuid
        });
      })
      .catch(err => console.error(err));

  }

};

exports.getOrders = async (req, res) => {

  let sql = "SELECT uuid, email FROM orders;";
  let result = await query(sql);

  res.render('admin/ordersList', { title: 'Orders', admin: true, orders: result.rows, type: 'order' });
};

exports.orderDetail = (req, res) => {};
