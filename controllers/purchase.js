// Purchase controller
const async = require('async');
const { check, validationResult } = require('express-validator');
const { Book } = require('../database/sequelize');
const { query } = require('../database/db');


exports.addToCart = (req, res) => {
  // TODO make this an async function?

  if (req.session.cart === undefined) {
    req.session.cart = {};
    req.session.cart[req.body.uuid] = { uuid: req.body.uuid, qty: Number(req.body.qty) };
    req.session.save();
    res.send({ uuid: req.body.uuid, qty: req.session.cart[req.body.uuid] });
  } else {
    if (req.session.cart[req.body.uuid] === undefined) {
      req.session.cart[req.body.uuid] = { uuid: req.body.uuid, qty: Number(req.body.qty) };
    } else {
      req.session.cart[req.body.uuid].qty += Number(req.body.qty);
    }
    req.session.save();
    res.send({ uuid: req.body.uuid, qty: req.session.cart[req.body.uuid] });
  }

};

exports.cartView = (req, res) => {

  async.map(req.session.cart, getBook, (err, cart) => {
    if (err) {
      req.flash('errors', 'Something went wrong.');
      res.redirect('/');
    } else {
      res.render('purchase/cart', { title: 'Cart', cart: cart });
    }
  });

};

exports.checkoutGet = (req, res) => {

  async.map(req.session.cart, getBook, (err, data) => {
    if (err) {
      req.flash("errors", "Something went wrong.");
      res.redirect("/");
    } else {
      if (data.length === 0) {
        req.flash("errors", "Your cart is empty!");
        res.redirect(req.originalUrl);
      } else {
        let cart = {};
        for (book in data) {
          cart[book.data.uuid] = book.qty;
        }
        res.render("purchase/checkout", { title: "Checkout", cart: cart })
      }
    }
  });

};

exports.checkoutPost = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let purchase = req.body;
    let address_vars = [
      purchase.shipping_first_name,
      purchase.shipping_last_name,
      purchase.shipping_address_1,
      purchase.shipping_address_2,
      purchase.shipping_city,
      purchase.shipping_region,
      purchase.shipping_zip,
    ];

    let payment_vars;

    if (purchase.billing_is_shipping) {
      payment_vars = [
        purchase.shipping_first_name + purchase.shipping_last_name,
        purchase.shipping_address_1,
        purchase.shipping_address_2,
        purchase.shipping_city,
        purchase.shipping_region,
        purchase.shipping_zip,
        purchase.card_number,
        purchase.card_exp,
        purchase.security_code,
      ];
    } else {
      payment_vars = [
        purchase.billing_name,
        purchase.billing_address_1,
        purchase.billing_address_2,
        purchase.billing_city,
        purchase.billing_region,
        purchase.billing_zip,
        purchase.card_number,
        purchase.card_exp,
        purchase.security_code,
      ];
    }
    let order_vars = [purchase.cart, '100'];

    let address_sql = "INSERT INTO addresses (first_name, last_name, address_line_1, address_line_2, city, state, zip_code) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING uuid;";
    let payment_sql = "INSERT INTO payments (name_on_card, address_line_1, address_line_2, city, state, zip_code, card_number, card_expiration, security_code) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING uuid;";
    let order_sql = "INSERT INTO orders (items, total_price, shipping, payment, auth_user) VALUES ($1, $2, $3, $4, $5);";

    try {

      let address = await query(address_sql, address_vars);
      let payment = await query(payment_sql, payment_vars);
      await query(order_sql, [...order_vars, address.rows[0].uuid, payment.rows[0].uuid, req.user]);

      res.redirect("/purchase/confirm");

    } catch (err) {

      req.flash("errors", "Something went wrong");
      res.redirect(req.originalUrl);

    }

  } else {

    req.flash("errors", "Something went wrong");
    res.redirect(req.originalUrl);

  }

};

exports.confirmGet = async (req, res) => {

  let sql = "SELECT * FROM orders FULL OUTER JOIN addresses a on orders.shipping = a.uuid WHERE user = $1 AND confirmed = FALSE;";

  try {
    let order = await query(sql, [req.user]);
    res.render('purchase/confirm', {title: "Confirm Order", order: order});
  } catch (err) {

    req.flash("errors", "Something went wrong");
    res.redirect(req.originalUrl);

  }

};

exports.confirmPost = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {
    let sql = "UPDATE orders SET confirmed = TRUE WHERE uuid = $1";

    try {
      query(sql, [req.uuid]);
    } catch (err) {
      req.flash("errors", "Something went wrong");
      res.redirect(req.originalUrl);
    }
  }

};

// Get each book in the user's cart
async function getBook(cart_obj) {
  let book = await Book.findByPk(cart_obj.uuid);
  return { data: book.dataValues, qty: cart_obj.qty }
}
