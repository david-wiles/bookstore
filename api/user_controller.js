const { validationResult } = require('express-validator');
const { User } = require('../database/sequelize');
const { query } = require('../database/db');


exports.getUser = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let user = await User.findByPk(req.params.uuid);
    res.send({ user: user });

  }

};

exports.searchUsers = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let query = req.query.text;
    let sql = "SELECT username, uuid FROM users WHERE email ILIKE $1;";

    let result = await query(sql, [query]);

    res.send({ result: result.rows });

  }

};

exports.postUser = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let user = await User.create(req.body.user);
    res.send({ user: user });

  }

};

exports.putUser = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let user = await User.update( req.body.item,
      { where: { uuid: req.body.uuid } }
    );

    res.send({ user: user });

  }

};

exports.deleteUser = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let result = await User.delete({ where: req.body.user });
    res.send({ success: result });

  }

};