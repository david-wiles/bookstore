const { check } = require('express-validator');
const express = require('express');
const router = express.Router();
const user_controller = require('./user_controller');
const group_controller = require('./group_controller');
const author_controller = require('./author_controller');
const book_controller = require('./book_controller');
const search_controller = require('./search_controller');
const { isAuthorized } = require('../config/auth');


router.all('*', isAuthorized);

// User
router.get('/user/:uuid', user_controller.getUser);
router.get('/user', user_controller.searchUsers);
router.post('/user', user_controller.postUser);
router.put('/user', user_controller.putUser);
router.delete('/user', user_controller.deleteUser);

// User groups
router.get('/auth_group/:uuid', group_controller.getGroup);
router.get('/auth_group', group_controller.searchGroups);
router.post('/auth_group', group_controller.postGroup);
router.put('/auth_group', group_controller.putGroup);
router.delete('/auth_group', group_controller.deleteGroup);

// Authors
router.get('/author/:uuid', author_controller.getAuthor);
router.get('/author', author_controller.searchAuthors);
router.post('/author', author_controller.postAuthor);
router.put('/author', author_controller.putAuthor);
router.delete('/author', author_controller.deleteAuthor);

// Books
router.get('/book/:uuid', book_controller.getBook);
router.get('/book', book_controller.searchBooks);
router.post('/book', book_controller.postBook);
router.put('/book', book_controller.putBook);
router.delete('/book', book_controller.deleteBook);

// Genres
// router.get();
// router.post();
// router.put();
// router.delete();

// Orders
// router.get();
// router.post();
// router.put();
// router.delete();

module.exports = router;