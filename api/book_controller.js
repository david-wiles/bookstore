const { validationResult } = require('express-validator');
const { Book } = require('../database/sequelize');

/**
 * CRUD + search endpoints for book objects
 */

exports.getBook = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let book = await Book.findByPk(req.params.uuid);
    res.send({book: book});

  }

};

exports.searchBooks = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let query = req.query.text;
    let sql = "SELECT title, uuid FROM books WHERE title ILIKE $1;";

    let result = await query(sql, [query]);

    res.send({ result: result.rows });

  }

};

exports.postBook = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    // todo ensure required fields are present
    let book = await Book.create(req.body.book);
    res.send({book: book});

  }

};

exports.putBook = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let book = await Book.update( req.body.item,
      { where: { uuid: req.body.uuid } }
    );
    res.send({book: book});

  }

};

exports.deleteBook = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let result = await Book.delete({where: req.body.book});
    res.send({success: result});

  }

};