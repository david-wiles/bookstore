const { validationResult } = require('express-validator');
const { Author } = require('../database/sequelize');

/**
 * CRUD + search endpoints for author objects
 */

exports.getAuthor = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let author = await Author.findByPk(req.params.uuid);
    res.send({author: author});

  }

};

exports.searchAuthors = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let query = req.query.text;
    let sql = "SELECT name, uuid FROM authors WHERE name ILIKE $1;";

    let result = await query(sql, [query]);

    res.send({ result: result.rows });

  }

};

exports.postAuthor = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    // todo ensure required fields are present
    let author = await Author.create(req.body.author);
    res.send({author: author});

  }

};

exports.putAuthor = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let author = await Author.update( req.body.item,
      {where: { uuid: req.body.uuid }}
    );

    res.send({author: author});

  }

};

exports.deleteAuthor = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let result = await Author.delete({where: req.body.author});
    res.send({success: result});

  }

};