const { validationResult } = require('express-validator');
const { Group } = require('../database/sequelize');


/**
 * CRUD + search endpoints for groups
 */

exports.getGroup = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let group = await Group.findByPk(req.params.uuid);
    res.send({group: group});

  }

};

exports.searchGroups = async (req, res) => {

  let errors = validationResult(req);

  if (errors.isEmpty()) {

    let query = req.query.text;
    let sql = "SELECT name, uuid FROM auth_groups WHERE name ILIKE $1;";

    let result = await query(sql, [query]);

    res.send({ result: result.rows });

  }

};

exports.postGroup = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    // todo ensure required fields are present
    let group = await Group.create(req.body.group);
    res.send({group: group});

  }

};

exports.putGroup = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let group = await Group.update( req.body.item,
      {where: { uuid: req.body.uuid } }
    );

    res.send({group: group});

  }

};

exports.deleteGroup = async (req, res) => {

  const errors = validationResult(req);

  if (errors.isEmpty()) {

    let result = await Group.delete({where: req.body.group});
    res.send({success: result});

  }

};