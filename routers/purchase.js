const { check } = require('express-validator');
const express = require('express');
const router = express.Router();
const purchaseController = require('../controllers/purchase');


router.get('/cart', purchaseController.cartView);

router.get('/checkout', purchaseController.checkoutGet);
router.post('/checkout', purchaseController.checkoutPost);

router.get('/confirm', purchaseController.confirmGet);
router.post('/confirm', purchaseController.confirmPost);

router.post('/addToCart', [], purchaseController.addToCart);

module.exports = router;