const { check } = require('express-validator');
const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users');
const passportConfig = require('../config/auth');


router.get('/account', passportConfig.isAuthenticated, usersController.accountGet);
router.put('/account', usersController.accountPut);

router.get('/logout', usersController.logout);

router.get('/login', usersController.loginGet);
router.post('/login', [
  check('email')
    .isEmail(),
  check('password')
    .isLength({ min: 8 })
    .isAscii(),
] ,usersController.loginPost);

router.get('/register', usersController.registerGet);
router.post('/register', [
  check('email')
    .isEmail().withMessage('Email must be valid.'),
  check('password')
    .isLength({ min: 8 }).withMessage('Password must be at least 8 characters.')
    .isAscii().withMessage('Password cannot contain special characters.'),
], usersController.registerPost);

router.get("/forgot", usersController.forgotPassword);
router.post("/forgot", usersController.sendEmail);

module.exports = router;
