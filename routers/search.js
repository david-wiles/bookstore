const { check } = require('express-validator');
const express = require('express');
const router = express.Router();
const searchController = require('../controllers/search');


router.get('/autocomplete', [], searchController.autocomplete);

module.exports = router;