const express = require('express');
const router = express.Router();
const adminController = require('../controllers/admin');
const { isAuthorized, isAdmin } = require('../config/auth');


router.all('*', isAdmin);
router.all('/admin/*', isAuthorized);


router.get('/', adminController.getIndex);

router.get('/book', adminController.getBooks);
router.get('/book/:uuid', adminController.bookDetail);

router.get('/author', adminController.getAuthors);
router.get('/author/:uuid', adminController.authorDetail);

router.get('/user', adminController.getUsers);
router.get('/user/:uuid', adminController.userDetail);

router.get('/auth_group', adminController.getGroups);
router.get('/auth_group/:uuid', adminController.groupDetail);

router.get('/order', adminController.getOrders);
router.get('/order/:uuid', adminController.orderDetail);

module.exports = router;