const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const catalogController = require('../controllers/catalog');


router.get('/', catalogController.catalog);

router.get('/search', catalogController.search);

router.get('/book', catalogController.bookIndex);
router.get('/book/:id', [
  // TODO check if is valid uuid
], catalogController.bookDetail);

router.get('/author', catalogController.authorIndex);
router.get('/author/:id',[
  // TODO check if is valid uuid
] , catalogController.authorDetail);

router.get('/keyword', catalogController.keyword);
router.get('/keyword/:id', catalogController.keywordDetail);

module.exports = router;
