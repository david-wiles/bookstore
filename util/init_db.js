const { Pool } = require('pg');
const dotenv = require('dotenv');
const fs = require('fs');
const { init } = require('../database/sequelize');

dotenv.config({ path: '.env' });
const schema = fs.readFileSync('../schema.sql', 'utf8');

init()
  .then(() => {
    const pool = new Pool();
    pool.query({ text: schema })
      .then(() => {
        console.log('Database created!');
        pool.end();
        process.exit();
      })
      .catch( err => console.error(err.stack));
  })
  .catch(err => console.error(err));
