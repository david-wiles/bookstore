const { User, Group } = require('../database/sequelize');
const { query } = require('../database/db');

const perms = {
  POST: 0,
  GET: 1,
  PUT: 2,
  DELETE: 3
};

const routes = {
  // TODO routes regex
};

exports.hasPermission = async (uuid, path, perm) => {

  let obj = path.substring(1);
  perm = perms[perm];

  let sql =
    `SELECT auth_groups.${obj} ` +
    `FROM users ` +
    `LEFT JOIN auth_groups on users.auth_group_uuid = auth_groups.uuid ` +
    `WHERE users.uuid = $1;`;

  try {

    let result = await query(sql, [uuid]);
    return result.rows[0][obj][perm];

  } catch (err) {

    console.error(err);
    return false;

  }

};