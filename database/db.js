const { Pool } = require('pg');
const pool = new Pool();
const redis = require('redis');
const client = redis.createClient(
  process.env.REDIS_PORT,
  process.env.REDIS_HOST
);

exports.query = (text, params) => {
  return pool.query(text, params);
};

exports.client = () => {
  return client;
};
