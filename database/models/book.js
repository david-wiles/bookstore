const { Sequelize, DataTypes } = require('sequelize');


module.exports = (sequelize) => {
  return sequelize.define('book', {

    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true,
      allowNull: false,
    },

    price: {
      type: Sequelize.NUMERIC(7, 2)
    },

    url: {
      type: Sequelize.STRING
    },

    title: {
      type: Sequelize.STRING
    },

    image: {
      type: Sequelize.STRING
    },

    author: {
      type: Sequelize.STRING
    },

    country: {
      type: Sequelize.STRING
    },

    language: {
      type: Sequelize.STRING
    },

    series: {
      type: Sequelize.STRING
    },

    genres: {
      type: Sequelize.STRING
    },

    publisher: {
      type: Sequelize.STRING
    },

    published: {
      type: Sequelize.STRING
    },

    media_type: {
      type: Sequelize.STRING
    },

    pages: {
      type: Sequelize.STRING
    },

    isbn: {
      type: Sequelize.STRING,
    },

    oclc: {
      type: Sequelize.STRING,
    },

    lc_class: {
      type: Sequelize.STRING,
    },

    dewey_decimal: {
      type: Sequelize.STRING,
    },

    preceded_by: {
      type: Sequelize.STRING,
    },

    followed_by: {
      type: Sequelize.STRING,
    },

    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },

    other: {
      type: DataTypes.JSON,
    }

  })
};