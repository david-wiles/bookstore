const { Sequelize, DataTypes } = require('sequelize');


module.exports = (sequelize) => {
  return sequelize.define('auth_group', {

    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1(),
      primaryKey: true,
      allowNull: false,
    },

    name: {
      type: Sequelize.STRING,
      unique: true,
      allowNull: false,
    },

    user: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    },

    auth_group: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    },

    book: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    },

    author: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    },

    order: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    },

    book_keyword: {
      type: Sequelize.ARRAY(Sequelize.BOOLEAN),
      defaultValue: [false, false, false, false],
      allowNull: false,
      validate: {
        isSpecificLength(value) {
          if (value.length !== 4) {
            throw new Error('Permission array must be length 4.')
          }
        }
      }
    }

  })
};