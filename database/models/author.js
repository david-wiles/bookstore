const { Sequelize, DataTypes } = require('sequelize');


module.exports = (sequelize) => {
  return sequelize.define('author', {

    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true,
      allowNull: false,
    },

    url: {
      type: Sequelize.STRING
    },

    image: {
      type: Sequelize.STRING
    },

    name: {
      type: Sequelize.STRING,
    },

    born: {
      type: Sequelize.STRING,
    },

    died: {
      type: Sequelize.STRING,
    },

    education: {
      type: Sequelize.STRING,
    },

    alma_mater: {
      type: Sequelize.STRING,
    },

    period: {
      type: Sequelize.STRING,
    },

    genres: {
      type: Sequelize.STRING,
    },

    spouse: {
      type: Sequelize.STRING,
    },

    children: {
      type: Sequelize.STRING
    },

    pen_name: {
      type: Sequelize.STRING
    },

    nationality: {
      type: Sequelize.STRING
    },

    occupation: {
      type: Sequelize.STRING
    },

    years_active: {
      type: Sequelize.STRING
    },

    subject: {
      type: Sequelize.STRING
    },

    notable_works: {
      type: Sequelize.STRING
    },

    notable_awards: {
      type: Sequelize.STRING
    },

    description: {
      type: Sequelize.STRING,
      allowNull: false,
    },

    other: {
      type: DataTypes.JSON,
    },

  })
};