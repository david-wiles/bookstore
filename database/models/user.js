const { Sequelize, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  return sequelize.define('user', {

    uuid: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV1,
      primaryKey: true,
      allowNull: false,
    },

    email: {
      type: Sequelize.STRING,
      allowNull: false,
      unique: true,
    },

    username: {
      type: Sequelize.STRING,
    },

    first_name: {
      type: Sequelize.STRING,
    },

    last_name: {
      type: Sequelize.STRING,
    },

    date_of_birth: {
      type: Sequelize.DATE,
    },

    street_address: {
      type: Sequelize.STRING,
    },

    city: {
      type: Sequelize.STRING(128),
    },

    state_code: {
      type: Sequelize.STRING(2),
    },

    zip_code: {
      type: Sequelize.INTEGER,
    },

    title: {
      type: Sequelize.STRING,
    },

    employment: {
      type: Sequelize.STRING,
    },

    password_hash: {
      type: Sequelize.STRING(60),
    },

  })
};
