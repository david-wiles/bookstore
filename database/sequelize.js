const { Sequelize } = require('sequelize');
const UserModel = require('./models/user');
const BookModel = require('./models/book');
const AuthorModel = require('./models/author');
const GroupModel = require('./models/auth_group');
const sequelize = new Sequelize(
  process.env.PGDATABASE,
  process.env.PGUSER,
  process.env.PGPASSWORD, {
    host: process.env.PGHOST,
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
  });

const User = UserModel(sequelize);
const Group = GroupModel(sequelize);
const Book = BookModel(sequelize);
const Author = AuthorModel(sequelize);

Author.hasMany(Book, {
  foreignKey: 'author_uuid'
});

Group.hasMany(User, {
  foreignKey: 'auth_group_uuid',
});


const init = () => {
  return new Promise(function(resolve, reject) {

    sequelize.sync({ force: true })
      .then(() => {
        console.log('Models created!');
        resolve();
      })
      .catch(err => reject(err));

  });
};

module.exports = { User, Group, Book, Author, init };