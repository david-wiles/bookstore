CREATE TABLE IF NOT EXISTS authors (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1() UNIQUE,
    url TEXT UNIQUE,
    name TEXT,
    image TEXT,
    born TEXT,
    died TEXT,
    education TEXT,
    alma_mater TEXT,
    occupation TEXT,
    period TEXT,
    genre TEXT,
    genres TEXT,
    spouse TEXT,
    children TEXT,
    pen_name TEXT,
    nationality TEXT,
    years_active TEXT,
    subject TEXT,
    notable_works TEXT,
    notable_awards TEXT,
    description TEXT,
    other json
);

CREATE TABLE IF NOT EXISTS books (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1() UNIQUE,
    author_uuid uuid REFERENCES authors(uuid),
    price NUMERIC (7, 2),
    url TEXT UNIQUE,
    title TEXT,
    image TEXT,
    author TEXT,
    country TEXT,
    language TEXT,
    series TEXT,
    genre TEXT,
    publisher TEXT,
    published TEXT,
    media_type TEXT,
    pages TEXT,
    isbn TEXT,
    oclc TEXT,
    lc_class TEXT,
    dewey_decimal TEXT,
    preceded_by TEXT,
    followed_by TEXT,
    description TEXT,
    other json
);

CREATE TABLE IF NOT EXISTS book_keywords (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
    description TEXT
);

CREATE TABLE IF NOT EXISTS orders (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
    items json,
    total_price NUMERIC,
    shipping uuid NOT NULL REFERENCES addresses(uuid),
    payment uuid NOT NULL REFERENCES payments(uuid),
    confirmed bool NOT NULL DEFAULT FALSE
);

CREATE TABLE IF NOT EXISTS addresses (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
    address_line_1 varchar(255) NOT NULL,
    address_line_2 varchar(255),
    city varchar(255) NOT NULL,
    state varchar(255) NOT NULL,
    zip_code NUMERIC (9) NOT NULL
);

CREATE TABLE IF NOT EXISTS payments (
    uuid uuid PRIMARY KEY DEFAULT uuid_generate_v1(),
    card_number NUMERIC (16) NOT NULL,
    card_expiration NUMERIC (4) NOT NULL,
    security_code NUMERIC NOT NULL,
    address_line_1 varchar(255) NOT NULL,
    address_line_2 varchar(255),
    city varchar(255) NOT NULL,
    state varchar(255) NOT NULL,
    zip_code NUMERIC (9) NOT NULL
);
