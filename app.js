const errorHandler = require('errorhandler');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const dotenv = require('dotenv');
const passport = require('passport');
const flash = require('express-flash');
const bodyParser = require('body-parser');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const redis = require('redis');
const sass = require('node-sass');


// Load environment variables
dotenv.config({ path: '.env' });

// Routes
const indexRouter = require('./routers/index');
const usersRouter = require('./routers/users');
const catalogRouter = require('./routers/catalog');
const adminRouter = require('./routers/admin');
const searchRouter = require('./routers/search');
const purchaseRouter = require('./routers/purchase');
const apiRouter = require('./api/router');

// Create server
const app = express();

// Set ip
app.set('host', process.env.NODE_IP || '127.0.0.1');

// View engine
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');
app.use(express.static('public'));
app.use(flash());
// app.use(
//   sass.middleware({
//     src: __dirname + '/sass',
//     dest: __dirname + '/public',
//     debug: true
//   })
// );

// Logger
app.use(logger('dev'));

// Bodyparser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Session
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge:  24 * 60 * 60 * 1000 }, // 24 hours
  store: new RedisStore({
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
  })
}));
app.use(cookieParser());

// Passport
app.use(passport.initialize());
app.use(passport.session());

// Disable headers
app.disable('x-powered-by');

// Routes
app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});
app.use((req, res, next) => { // Redirect after login
  if (!req.user
    && req.path !== '/users/login'
    && req.path !== '/users/register') {
    req.session.returnTo = req.originalUrl;
  }
  next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/catalog', catalogRouter);
app.use('/admin', adminRouter);
app.use('/search', searchRouter);
app.use('/purchase', purchaseRouter);
app.use('/api', apiRouter);

// Error handler
if (process.env.NODE_ENV === 'development') {
  app.use(errorHandler());
} else {
  app.use( (err, req, res, next) => {
    // console.error(err);
    res.status(500).send('Server Error');
  });
}

module.exports = app;
