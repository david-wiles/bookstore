let cart_btn;

$(function() {
  cart_btn = $("#add-to-cart");

  cart_btn.click(() => {

    $.post({
      data: { uuid: $("#uuid").val() , qty: 1 },
      url: "/purchase/addToCart",
    }).done(result => displayResult(result));

  });
});

function displayResult(result) {
  console.log(result);
}