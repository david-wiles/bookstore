let search;
let form;

$(function () {
  search = $("#search-box");
  form = $("#search-form");

  search.keyup(delayAction(200, function(){
    let text = search.val();

    // Remove results if user clears input box
    if (!text) {
      $('.search-results-container').remove();
      $("#search-box").removeClass('search-box-active');
    } else {
      $.get({
        data: { text: text },
        url: '/search/autocomplete',
      }).done(response => autoComplete(response));
    }
  }));

});

/* Delays a function for a set amount of time,
 * and prevents multiple function calls before timeout
 */
function delayAction(ms, fn) {
  let timer;
  return function (...args) {
    clearTimeout(timer);
    timer = setTimeout(fn.bind(this, ...args), ms);
  }
}

// Append the results of search query into a div below the search box
function autoComplete(response) {

  // Remove the results div before adding more results
  $('.search-results-container').remove();
  $("#search-box").removeClass('search-box-active');

  if (response.length > 0) {
    if (!$('.search-results-container').length > 0) {
      $(".search-row").append(
        "<div class='col col-8 search-results-container'></div>"
      );
      $("#search-box").addClass('search-box-active');
    }

    response.forEach(row => {
      let name = row['title'];
      let uuid = row['uuid'];

      $(".search-results-container").append(
        "<div class='search-result' data-id=" + uuid + ">" +
          name +
        "</div>"
      );

      $(".search-result").click(function() {
        let id = $(this).data('id');
        location.href = "/catalog/book/" + id;
      })
    });
  }
}
