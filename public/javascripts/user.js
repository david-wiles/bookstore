let submit;

$(function() {

  submit = $("#submit_account");
  let data = {};

  submit.click(() => {

    $("#user_account")
      .find("input")
      .each(function() {

        if ($(this).val().length > 0) {
          data[$(this).attr('id')] = $(this).val();
        }

      });

    $.ajax('/user/account', {
      type: 'PUT',
      data: data,
    }).done(()=>alert('Account updated'));
  })

});