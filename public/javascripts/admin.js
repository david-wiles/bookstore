let search;
let search_form;
let update;
let update_form;

$(function () {

  // Main search
  search = $("#search-box");
  search_form = $("#search-form");

  search.keyup(delayAction(200, function(){
    let text = search.val();

    // Remove results if user clears input box
    if (!text) {
      $('.search-results-container').remove();
      $("#search-box").removeClass('search-box-active');
    } else {
      $.get({
        data: { text: text },
        url: '/search/autocomplete',
      }).done(response => autoComplete(response));
    }
  }));

  // Card links
  $('.admin-link').click(function(click) {
    var loc = $(click.currentTarget).attr('loc');
    window.location.href = '/admin/' + loc;
  });


  // Update form
  update = $("#update_item");
  update_form = $("#update_form");

  update.click(() => {
    let item = {};

    update_form
      .find('.form-group')
      .each(function () {
        // For groups, iterate through each array of checkboxes
        if ($(this).children('ul.list-group').length > 0) {
          var perms = [];
          var list = $(this).children('ul.list-group');

          list
            .find('input')
            .each(function() {
              ($(this).is(':checked')) ?
                perms.push(true) :
                perms.push(false);
            });

          item[list.attr('name')] = perms;

        } else {
          var input = $(this).children('input');

          if (input.val().length > 0) {
            item[input.attr('name')] = input.val();
          }
        }
      });

    $.ajax('/api/' + update_form.data('type'), {
      type: 'PUT',
      data: {
        uuid: update_form.data('uuid'),
        item: item
      },
    }).done(() => alert('updated'));
  })

});

/* Delays a function for a set amount of time,
 * and prevents multiple function calls before timeout
 */
function delayAction(ms, fn) {
  let timer;
  return function (...args) {
    clearTimeout(timer);
    timer = setTimeout(fn.bind(this, ...args), ms);
  }
}

// Append the results of search query into a div below the search box
function autoComplete(response) {

  // Remove the results div before adding more results
  $('.search-results-container').remove();
  $("#search-box").removeClass('search-box-active');

  if (response.length > 0) {
    if (!$('.search-results-container').length > 0) {
      $(".search-row").append(
        "<div class='col col-8 search-results-container'></div>"
      );
      $("#search-box").addClass('search-box-active');
    }

    response.forEach(row => {
      let name = row['title'];

      $(".search-results-container").append(
        "<div class='search-result' onclick='submitElement(this);'>" +
        name +
        "</div>"
      );
    });
  }
}

function submitElement(element) {
  $("#search-box").val($(element).text());
  search_form.submit();
}

